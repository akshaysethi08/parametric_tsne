# Visualization Implementations

Collections of various useful visualization implementations

### TSNE

* SKlearn implementation - contains both an exact and approximate implementation of tsne 
* Does not scale to large datasets 
* https://scikit-learn.org/stable/modules/generated/sklearn.manifold.TSNE.html

### Parametric TSNE 

* Parametric version of TSNE, much more scalable as a NN is used as the functional approximator 
* At https://bitbucket.org/akshaysethi08/deep_vis/src/master/param_tsne.py
* Implementation from https://github.com/zaburo-ch/Parametric-t-SNE-in-Keras/blob/master/mlp_param_tsne.py

### UMAP 
* Dimensionality Reduction based on Riemann Geometry and Abstract Algebra
* Competitive Results and Faster ( in compute time ) to Parametric TSNE 
* https://github.com/lmcinnes/umap

